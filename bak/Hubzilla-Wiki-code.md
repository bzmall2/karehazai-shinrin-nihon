### 日本国有林の枯葉剤埋設地について: 
https://zotum.net/wiki/bsmall2/DigitalGarden/KareHaZaiShinRinNiHon

  1. 全国国有林の番組(NHK)と記事(週間金曜日)
     - 全国： Hubzilla WePage[1]
     - 宮崎県周辺の部分[2]
  2. 林野庁　情報公開資料 (6.7MB 〜 14.7MB .pdfs)
     - [3a] R2年度前期分： 令和2年04月22日〜  R2.4 
     - [3b] R2年度後期分： 令和2年11月19日〜 R2.10
       - p.26重永国有林の傾斜面はとくに危ないか？
     - [3c] R2年度臨時分： 令和3年02月01日〜  R2èB
       - p.36重永は傾斜面の尾根に見える。あいうところがいつ崩れてもびっくりしない。
     - [3d] R3年度前期分： 令和3年05月13日〜  R3.4
       - p.33都城市高崎の地図、黒塗り。
     - [3e] R3年度後期分： 令和3年11月10日〜 R3.10 
     - [3f]R3年度臨時分 ：令和3年8月24日〜  R3臨時 
     


### End Notes
 - [1] [https://zotum.net/page/bsmall2/zenkoku-kokuyuurin-dioxins](https://zotum.net/page/bsmall2/zenkoku-kokuyuurin-dioxins)
 - [2] [https://zotum.net/page/bsmall2/miyazaki-no-karehazai](https://zotum.net/page/bsmall2/miyazaki-no-karehazai)
 - [3] 林野庁　情報公開資料
   - [3a] 令和2年4月22日〜 ( [9.6MB .pdf](https://zotum.net/cloud/bsmall2/karehazai-shinrin-nihon/R2%E5%B9%B4%E5%BA%A6.%E5%89%8D%E6%9C%9F%E5%88%86.pdf) )  
   - [3b] 令和2年11月19日〜 ( [10.4MB .pdf](https://zotum.net/cloud/bsmall2/karehazai-shinrin-nihon/R2%E5%B9%B4%E5%BA%A6.%E5%BE%8C%E6%9C%9F%E5%88%86.pdf) )
   - [3c] 令和3年02月01日〜 ([13.8MB .pdf](https://zotum.net/cloud/bsmall2/karehazai-shinrin-nihon/R2%E5%B9%B4%E5%BA%A6.%E8%87%A8%E6%99%82%E5%88%86.pdf) )
   - [3d] 令和3年05月13日〜 ( [9.2MB .pdf](https://zotum.net/cloud/bsmall2/karehazai-shinrin-nihon/R3%E5%B9%B4%E5%BA%A6.%E5%89%8D%E6%9C%9F%E5%88%86.pdf) )
   - [3e] 令和3年11月10日〜   ( [14.7MB](https://zotum.net/cloud/bsmall2/karehazai-shinrin-nihon/R3%E5%B9%B4%E5%BA%A6.%E5%BE%8C%E6%9C%9F%E5%88%86.pdf) )
   - [3f] 令和3年8月24日〜 ( [6.7MB .pdf](https://zotum.net/cloud/bsmall2/karehazai-shinrin-nihon/R3%E5%B9%B4%E5%BA%A6.%E8%87%A8%E6%99%82%E5%88%86.pdf) )
